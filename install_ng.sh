#!/bin/bash
######################################################
# Script Instalação do Lyceum NG                     #
# Criado por Fillipe Alves                           #
# Data 13/05/2021                                    #
# Sistema Operacional: Ubuntu Server                 #
# Versão do Script: 1.0				     #
######################################################

echo "Inicio"

echo "Parando Tomcat"
systemctl stop tomcat.service

echo "Limpando cache"
rm -rf /opt/techne/lyceum/tomcat/logs/*
rm -rf /opt/techne/lyceum/tomcat/work/*
rm -rf /opt/techne/lyceum/tomcat/temp/*
rm -rf /opt/techne/lyceum/tomcat/webapps/*/

echo "Corrigindo parâmetros do Lyceum"
perl -p -i -e "s/\r//g" /root/.config/lyceum/parameters.sh

echo "Removendo pasta de Recursos"
rm -rf /opt/techne/lyceum/resources/

echo "Entrando no diretório informado"
cd /opt/techne/lyceum/$1/bin/

echo "Declarando variável de ambiente CATALINA_HOME"
export CATALINA_HOME=/opt/techne/lyceum/tomcat/

echo "Executando o setup do Lyceum"
./setup

echo "Aplicando permissão de usuário Lyceum e grupo Techne nas pastas"
chown lyceum.techne -R /opt/techne/lyceum/

echo ""
read -p "Deseja iniciar o Tomcat? [y/n]" resposta
case $resposta in
	[Yy]* )
	echo "Iniciando o tomcat..."
	systemctl start tomcat.service
	echo "Fim!"
	break;;
	[Nn]* )
	echo "Tomcat não iniciado."
	echo "Fim!"
	break;;
	*)
	echo "Opção inválida"
	echo "Tomcat não iniciado."
	echo "Fim"
esac
